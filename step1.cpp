#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <panel.h>
#include <curses.h>
#include <dirent.h>
#include <string.h>
#include <menu.h>

int main(){	
	
	PANEL  *panels[2];
	WINDOW *windows[2];
	
	int token;
	int lines = 60, cols = 60;
        int count = 0;
        int y = 0, x = 0;
        
        cbreak();
	noecho();
	initscr();

	start_color();
        init_pair(1, COLOR_RED, COLOR_BLACK);
        init_pair(2, COLOR_RED, COLOR_BLACK);

	windows[0] = newwin(lines, cols, y, x);
	windows[1] = newwin(lines, cols, y, x + cols + 4);
	
	wborder(windows[0], '*', '*', '*', '*', '*', '*', '*', '*');
	wborder(windows[1], '*', '*', '*', '*', '*', '*', '*', '*');

	for(count; count<=1; ++count) {
		box(windows[count], 0, 0);
	}

	panels[0] = new_panel(windows[0]); 	
	panels[1] = new_panel(windows[1]); 	
	
	update_panels();
	doupdate();
	
	mvwaddstr(windows[0], 0, 0, "Type commands here (use ctrl+z for exit)\n========================================\n");
	wbkgd(windows[0], COLOR_PAIR(1));
	
	mvwaddstr(windows[1], 0, 0, "Commands taken\n==============\n");
	wbkgd(windows[1], COLOR_PAIR(1));
	
	wrefresh(windows[0]);
	wrefresh(windows[1]);
	
	token = wgetch(windows[0]);
	
	
	while (token != 26) {
		if (token != ' ') {
			waddch(windows[0], token);
			wrefresh(windows[0]);
			
			waddch(windows[1], token);
			wrefresh(windows[1]);
			
			update_panels();
			doupdate();
		}
		else {
			waddch(windows[0], token);
			waddch(windows[1], '\n');
			
			wrefresh(windows[0]);	
			wrefresh(windows[1]);
			
			update_panels();
			doupdate();

		}
		
		token = wgetch(windows[0]);
		
	}
	
	endwin();
	
	return 0;
}